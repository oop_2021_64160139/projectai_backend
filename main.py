from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
import pandas as pd
import joblib
from fastapi.middleware.cors import CORSMiddleware
# โหลดโมเดล
model = joblib.load('random_forest_model.pkl')

# กำหนดโมเดลของข้อมูล
class EmployeeData(BaseModel):
    Education: str
    JoiningYear: int
    City: str
    PaymentTier: int
    Age: int
    Gender: str
    EverBenched: str
    ExperienceInCurrentDomain: int

app = FastAPI()
app.add_middleware(
    CORSMiddleware, # type: ignore
    allow_origins=["*"],  # allow_origins กำหนดให้ทุก origin สามารถเรียกใช้ API ได้
    allow_credentials=True, # allow_credentials ให้อนุญาตให้ส่งข้อมูล credentials ระหว่าง origin ต่าง ๆ
    allow_methods=["GET","POST","PUT","DELETE"],  
    # allow_methods กำหนดเมทอดที่อนุญาตให้ใช้งานกับ API ได้ (GET, POST, PUT, DELETE)
    allow_headers=["*"]  
    # allow_headers กำหนดส่วนหัวที่อนุญาตให้ใช้งานกับ API ได้ (ให้ใช้ทุกส่วนหัว)
)
@app.post("/predict")
async def predict(employee_data: EmployeeData):
    # แปลงข้อมูลใหม่เป็น DataFrame
    new_data = pd.DataFrame(employee_data.dict(), index=[0])
    new_data = pd.get_dummies(new_data)
    
    # ตรวจสอบคอลัมน์ที่ขาดหายไปในข้อมูลทดสอบ
    missing_cols = set(model.feature_importances_) - set(new_data.columns)

    # เพิ่มคอลัมน์ที่ขาดหายไปเข้าไปในข้อมูลทดสอบ
    for col in missing_cols:
        new_data[col] = 0

    # ตรวจสอบลำดับของคอลัมน์ในข้อมูลทดสอบ
    new_data = new_data[model.feature_importances_]

    # ทำการทำนายผล
    prediction = model.predict(new_data)

    return {"prediction": int(prediction[0])}
